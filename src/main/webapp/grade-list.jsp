<%@ page import="java.util.*" %>
<%@ page import="pl.kliniewski.schoolregister.grade.Grade" %>
<%@ page import="pl.kliniewski.schoolregister.subject.Subject" %>
<%@ page contentType="text/html" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>

    <title>Lista ocen</title>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>
<h1>Lista ocen</h1>

<%
    if (request.getAttribute("deleteSuccess") != null)
    {
        out.println("<p>Ocena została prawidłowo usunięta</p>");
    }
%>

<table>
    <tr>
        <th>Id</th>
        <th>Ocena</th>
        <th>Data nadania</th>
        <th>Akcja</th>
    </tr>
    <%
        Collection<Grade> grades = (Collection<Grade>) request.getAttribute("grades");
        if (grades.isEmpty())
        {
            out.println("<tr>");
            out.println("<td style=\"text-align: center;\" colspan=\"4\">Brak ocen</td>");
            out.println("</tr>");
        }
        for (Grade grade : grades)
        {
            out.println("<tr>");
            out.println("<td>" + grade.getId() + "</td>");
            out.println("<td>" + grade.getValue() + "</td>");
            out.println("<td>" + grade.getDateOfIssued() + "</td>");
            out.println("<td>");
            out.println("<form method=\"POST\">");
            out.println("<input name=\"delete\" type=\"hidden\" value=\"true\" />");
            out.println("<input name=\"id\" type=\"hidden\" value=\"" + grade.getId() + "\" />");
            out.println("<button type=\"submit\">Usuń ocenę</button>");
            out.println("</form>");
            out.println("</td>");
            out.println("</tr>");
        }
    %>
</table>
<form action="grades-average" method="GET">
    <%
        Subject subject = (Subject) request.getAttribute("subject");
        out.println("<input name=\"subject-id\" type=\"hidden\" value=\"" + subject.getId() + "\" />");
    %>
    <button type="submit">Sprawdź średnią ocen</button>
</form>

<%
    if (request.getAttribute("badNumberFormat") != null)
    {
        out.println("<p>Podano nieprawidłowy format oceny</p>");
    }
%>

<form method="POST">
    <label>
        Ocena:
        <input name="value" type="number" step="0.5" placeholder="Podaj ocenę">
    </label>

    <button type="submit">Dodaj nową ocenę</button>
</form>

<form method="GET" action="subject-list">
    <button type="submit">Wyświetl listę przedmiotów</button>
</form>

</body>
</html>