<%@ page import="pl.kliniewski.schoolregister.subject.Subject" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Średnia ocen</title>
</head>
<body>

<%
    Object lastGradesAverage = request.getAttribute("lastGradesAverage");
    if (lastGradesAverage != null)
    {
        out.println("Ostatnio wyliczona średnia zapisana w ciasteczkach: " + lastGradesAverage);
    }
%>

<p>
    <%
        if (request.getAttribute("gradesAverageError") != null)
        {
            out.println("Wystąpił błąd podczas obliczania średniej ocen");
        }
        else
        {
            out.println("Średnia ocen: " + request.getAttribute("gradesAverage") + "");
        }
    %>
</p>

<form action="grade-list" method="GET">
    <%
        Subject subject = (Subject) request.getAttribute("subject");
        out.println("<input name=\"subject-id\" type=\"hidden\" value=\"" + subject.getId() + "\" />");
    %>
    <button type="submit">Powróć do listy ocen</button>
</form>
</body>
</html>
