<%@ page import="java.util.*" %>
<%@ page import="pl.kliniewski.schoolregister.subject.Subject" %>
<%@ page contentType="text/html" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>

    <title>Lista przedmiotów</title>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>
<h1>Lista przedmiotów</h1>

<%
    if (request.getAttribute("deleteSuccess") != null)
    {
        out.println("<p>Przedmiot został prawidłowo usunięty</p>");
    }
%>

<table>
    <tr>
        <th>Id</th>
        <th>Nazwa</th>
        <th>Liczba ocen</th>
        <th>Akcja</th>
    </tr>
    <%
        Collection<Subject> subjects = (Collection<Subject>) request.getAttribute("subjects");
        if (subjects.isEmpty())
        {
            out.println("<tr>");
            out.println("<td style=\"text-align: center;\" colspan=\"4\">Brak przedmiotów</td>");
            out.println("</tr>");
        }
        for (Subject subject : subjects)
        {
            out.println("<tr>");
            out.println("<td>" + subject.getId() + "</td>");
            out.println("<td>" + subject.getName() + "</td>");
            out.println("<td>" + subject.getGrades().size() + "</td>");
            out.println("<td>");
            out.println("<form method=\"POST\">");
            out.println("<input name=\"delete\" type=\"hidden\" value=\"true\" />");
            out.println("<input name=\"id\" type=\"hidden\" value=\"" + subject.getId() + "\" />");
            out.println("<button type=\"submit\">Usuń przedmiot</button>");
            out.println("</form>");
            out.println("<form method=\"GET\" action=\"grade-list\">");
            out.println("<input name=\"subject-id\" type=\"hidden\" value=\"" + subject.getId() + "\" />");
            out.println("<button type=\"submit\">Sprawdź oceny</button>");
            out.println("</form>");
            out.println("</td>");
            out.println("</tr>");
        }
    %>
</table>

<form method="POST">
    <label>
        Nazwa przedmiotu:
        <input name="name" type="text" placeholder="Podaj nazwę przedmiotu">
    </label>

    <button type="submit">Dodaj nowy przedmiot</button>
</form>

<p>
    <%
        out.println("Sesja podtrzymana od: " + request.getAttribute("sessionCreatedAt"));
    %>
</p>

</body>
</html>