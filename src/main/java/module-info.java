module pl.kliniewski.schoolregister {
    requires javafx.controls;
    requires javafx.fxml;
    requires javax.servlet.api;
    requires java.persistence;
    requires java.compiler;
    requires derby;

    opens pl.kliniewski.schoolregister to javafx.fxml;
    opens pl.kliniewski.schoolregister.grade.controller.gui to javafx.fxml;
    exports pl.kliniewski.schoolregister;
    exports pl.kliniewski.schoolregister.grade;
    exports pl.kliniewski.schoolregister.grade.repository;
    exports pl.kliniewski.schoolregister.grade.controller.gui;
}