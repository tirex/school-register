package pl.kliniewski.schoolregister;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pl.kliniewski.schoolregister.grade.GradeModel;
import pl.kliniewski.schoolregister.grade.controller.gui.GradeViewController;
import pl.kliniewski.schoolregister.grade.repository.GradeInMemoryRepository;
import pl.kliniewski.schoolregister.grade.repository.GradeRepository;
import pl.kliniewski.schoolregister.grade.view.gui.GradeGuiViewLocation;

import java.time.format.DateTimeFormatter;

/**
 * Start point of gui application
 */
public class SchoolRegisterGuiApplication
        extends Application
{
    @Override
    public void start(Stage stage) throws Exception
    {
        GradeRepository gradeRepository = new GradeInMemoryRepository();

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm dd.MM.yyyy");
        GradeModel gradeModel = new GradeModel();
        GradeViewController controller = new GradeViewController(gradeModel, gradeRepository, dateTimeFormatter);

        FXMLLoader loader = new FXMLLoader(GradeGuiViewLocation.class.getResource("grade-view.fxml"));
        loader.setController(controller);

        Parent parent = loader.load();
        Scene scene = new Scene(parent, 640, 360);
        stage.setTitle("School Register");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args)
    {
        launch(args);
    }
}
