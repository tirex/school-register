package pl.kliniewski.schoolregister.subject.repository;

import pl.kliniewski.schoolregister.Repository;
import pl.kliniewski.schoolregister.subject.Subject;

public interface SubjectRepository
        extends Repository<Integer, Subject>
{
}
