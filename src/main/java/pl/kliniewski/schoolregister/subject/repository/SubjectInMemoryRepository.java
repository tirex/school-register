package pl.kliniewski.schoolregister.subject.repository;

import pl.kliniewski.schoolregister.grade.Grade;
import pl.kliniewski.schoolregister.grade.repository.GradeRepository;
import pl.kliniewski.schoolregister.subject.Subject;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Implementation of subject in memory repository
 */
public class SubjectInMemoryRepository
        implements SubjectRepository
{
    /**
     * Map which stores subject map by id
     */
    private final Map<Integer, Subject> subjectMap = new HashMap<>();

    /**
     * Next subject id to use when saving
     */
    private int nextSubjectId = 1;

    private final GradeRepository gradeRepository;

    public SubjectInMemoryRepository(GradeRepository gradeRepository)
    {
        this.gradeRepository = Objects.requireNonNull(gradeRepository, "gradeRepository");
    }

    @Override
    public Collection<Subject> findAll()
    {
        return this.subjectMap.values();
    }

    @Override
    public Subject findById(Integer id)
    {
        return this.subjectMap.get(id);
    }

    @Override
    public void save(Subject value)
    {
        if (!this.subjectMap.containsValue(value))
        {
            value.setId(this.nextSubjectId++);
            this.subjectMap.put(value.getId(), value);
        }
    }

    @Override
    public void deleteById(Integer id)
    {
        Subject subject = this.findById(id);
        if (subject != null)
        {
            this.delete(subject);
        }
    }

    @Override
    public void delete(Subject value)
    {
        for (Grade grade : value.getGrades())
        {
            grade.setSubject(null);
            this.gradeRepository.delete(grade);
        }
        this.subjectMap.remove(value.getId());
    }
}
