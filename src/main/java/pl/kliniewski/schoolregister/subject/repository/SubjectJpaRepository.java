package pl.kliniewski.schoolregister.subject.repository;

import pl.kliniewski.schoolregister.subject.Subject;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collection;
import java.util.Objects;

public class SubjectJpaRepository
        implements SubjectRepository
{
    private final EntityManager entityManager;

    public SubjectJpaRepository(EntityManager entityManager)
    {
        this.entityManager = Objects.requireNonNull(entityManager, "entityManager");
    }

    @Override
    public Collection<Subject> findAll()
    {
        CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();
        CriteriaQuery<Subject> query = cb.createQuery(Subject.class);
        Root<Subject> root = query.from(Subject.class);
        CriteriaQuery<Subject> all = query.select(root);
        TypedQuery<Subject> typedQuery = this.entityManager.createQuery(all);
        return typedQuery.getResultList();
    }

    @Override
    public Subject findById(Integer id)
    {
        return this.entityManager.find(Subject.class, id);
    }

    @Override
    public void save(Subject value)
    {
        this.entityManager.getTransaction().begin();
        try
        {
            this.entityManager.persist(value);
            this.entityManager.getTransaction().commit();
        }
        catch (PersistenceException e)
        {
            e.printStackTrace();
            this.entityManager.getTransaction().rollback();
        }
    }

    @Override
    public void deleteById(Integer id)
    {
        this.entityManager.getTransaction().begin();
        try
        {
            this.entityManager.remove(this.entityManager.find(Subject.class, id));
            this.entityManager.getTransaction().commit();
        }
        catch (PersistenceException e)
        {
            e.printStackTrace();
            this.entityManager.getTransaction().rollback();
        }
    }

    @Override
    public void delete(Subject value)
    {
        this.entityManager.getTransaction().begin();
        try
        {
            this.entityManager.remove(value);
            this.entityManager.getTransaction().commit();
        }
        catch (PersistenceException e)
        {
            e.printStackTrace();
            this.entityManager.getTransaction().rollback();
        }
    }
}
