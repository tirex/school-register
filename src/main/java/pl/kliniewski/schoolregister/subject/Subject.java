package pl.kliniewski.schoolregister.subject;

import pl.kliniewski.schoolregister.grade.Grade;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

/**
 * This class is container of subject
 */
@Entity
public class Subject
{
    /**
     * Subject id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * Subject name
     */
    private String name;

    /**
     * Subject collection of grades
     */
    @OneToMany(mappedBy = "subject")
    private final Collection<Grade> grades = new ArrayList<>();

    public Subject()
    {
        this(0, null);
    }

    public Subject(int id, String name)
    {
        this.id = id;
        this.name = name;
    }

    /**
     * Subject id getter
     *
     * @return subject id
     */
    public int getId()
    {
        return id;
    }

    /**
     * Subject id setter
     */
    public void setId(int id)
    {
        this.id = id;
    }

    /**
     * Subject name getter
     *
     * @return subject name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Subject name setter
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Subject grades getter
     *
     * @return subject grades
     */
    public Collection<Grade> getGrades()
    {
        return grades;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (!(o instanceof Subject))
        {
            return false;
        }
        Subject subject = (Subject) o;
        return id == subject.id;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id);
    }

    @Override
    public String toString()
    {
        return "Subject{" + "id=" + id + ", name='" + name + '\'' + '}';
    }
}
