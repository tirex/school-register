package pl.kliniewski.schoolregister.subject.controller.web;

import pl.kliniewski.schoolregister.grade.repository.GradeRepository;
import pl.kliniewski.schoolregister.subject.Subject;
import pl.kliniewski.schoolregister.subject.repository.SubjectRepository;
import pl.kliniewski.schoolregister.util.ServletUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDateTime;

@WebServlet(name = "subjectListServlet", urlPatterns = "/subject-list")
public class SubjectListServlet
        extends HttpServlet
{
    private SubjectRepository subjectRepository;

    @Override
    public void init(ServletConfig config)
    {
        ServletContext servletContext = config.getServletContext();
        GradeRepository gradeRepository = ServletUtils.getGradeRepository(servletContext);
        this.subjectRepository = ServletUtils.getSubjectRepository(servletContext, gradeRepository);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        if (session.getAttribute("created") == null)
        {
            session.setAttribute("created", LocalDateTime.now());
        }
        request.setAttribute("sessionCreatedAt", session.getAttribute("created"));

        request.setAttribute("subjects", this.subjectRepository.findAll());

        RequestDispatcher dispatcher = request.getRequestDispatcher("subject-list.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if (request.getParameter("delete") != null)
        {
            String idText = request.getParameter("id");
            int id = Integer.parseInt(idText);
            this.subjectRepository.deleteById(id);

            request.setAttribute("deleteSuccess", true);
            this.doGet(request, response);
            return;
        }
        String name = request.getParameter("name");
        Subject subject = new Subject(0, name);
        this.subjectRepository.save(subject);
        this.doGet(request, response);
    }
}
