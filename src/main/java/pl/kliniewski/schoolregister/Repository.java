package pl.kliniewski.schoolregister;

import java.util.Collection;

/**
 * Repository which stores and get value elements
 *
 * @param <K> value key
 * @param <T> value type
 */
public interface Repository<K, T>
{
    /**
     * Find all elements in repository
     *
     * @return collection of elements
     */
    Collection<T> findAll();

    /**
     * Find one element by id
     *
     * @param id element id
     * @return founded element
     */
    T findById(K id);

    /**
     * Save one element
     *
     * @param value element to be saved
     */
    void save(T value);

    /**
     * Delete one element by id
     *
     * @param id element id to be deleted
     */
    void deleteById(K id);

    /**
     * Delete one element
     *
     * @param value element to be deleted
     */
    void delete(T value);
}
