package pl.kliniewski.schoolregister;

import pl.kliniewski.schoolregister.grade.GradeModel;
import pl.kliniewski.schoolregister.grade.controller.console.GradeListController;
import pl.kliniewski.schoolregister.grade.repository.GradeInMemoryRepository;
import pl.kliniewski.schoolregister.grade.repository.GradeRepository;
import pl.kliniewski.schoolregister.grade.view.console.GradeListView;

import java.time.format.DateTimeFormatter;

/**
 * Start point of console application
 */
public class SchoolRegisterConsoleApplication
{
    public static void main(String[] args)
    {
        GradeRepository gradeRepository = new GradeInMemoryRepository();

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm dd.MM.yyyy");
        GradeListView view = new GradeListView(dateTimeFormatter);
        GradeModel gradeModel = new GradeModel();
        GradeListController controller = new GradeListController(view, gradeModel, gradeRepository);

        controller.start();
    }
}
