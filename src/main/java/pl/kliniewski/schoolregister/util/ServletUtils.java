package pl.kliniewski.schoolregister.util;

import pl.kliniewski.schoolregister.grade.GradeModel;
import pl.kliniewski.schoolregister.grade.repository.GradeJpaRepository;
import pl.kliniewski.schoolregister.grade.repository.GradeRepository;
import pl.kliniewski.schoolregister.subject.Subject;
import pl.kliniewski.schoolregister.subject.repository.SubjectJpaRepository;
import pl.kliniewski.schoolregister.subject.repository.SubjectRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * Servlet utils
 */
public class ServletUtils
{
    /**
     * Helper method to easily get or create entity manager
     *
     * @param context current servler context
     * @return unique entity manger for this context
     */
    public static EntityManager getEntityManager(ServletContext context)
    {
        Object attribute = context.getAttribute("entityManager");
        if (!(attribute instanceof EntityManager))
        {
            attribute = null;
        }
        EntityManager entityManager = (EntityManager) attribute;
        if (entityManager == null)
        {
            EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
            entityManager = entityManagerFactory.createEntityManager();
            context.setAttribute("entityManager", entityManager);
        }
        return entityManager;
    }

    /**
     * Helper method to easily get or create grade repository
     *
     * @param context current servlet context
     * @return unique grade repository for this context
     */
    public static GradeRepository getGradeRepository(ServletContext context)
    {
        Object attribute = context.getAttribute("gradeRepository");
        if (!(attribute instanceof GradeRepository))
        {
            attribute = null;
        }
        GradeRepository repository = (GradeRepository) attribute;
        if (repository == null)
        {
            repository = new GradeJpaRepository(getEntityManager(context));
            context.setAttribute("gradeRepository", context);
        }
        return repository;
    }

    /**
     * Helper method to easily get or create subject repository
     *
     * @param context current servlet context
     * @return unique subject repository for this context
     */
    public static SubjectRepository getSubjectRepository(ServletContext context, GradeRepository gradeRepository)
    {
        Object attribute = context.getAttribute("subjectRepository");
        if (!(attribute instanceof SubjectRepository))
        {
            attribute = null;
        }
        SubjectRepository repository = (SubjectRepository) attribute;
        if (repository == null)
        {
            repository = new SubjectJpaRepository(getEntityManager(context));
            context.setAttribute("subjectRepository", repository);
        }
        return repository;
    }

    /**
     * Helper method to easily get or create grade model
     *
     * @param context current servlet context
     * @return unique grade model for this context
     */
    public static GradeModel getGradeModel(ServletContext context)
    {
        Object attribute = context.getAttribute("gradeModel");
        if (!(attribute instanceof GradeModel))
        {
            attribute = null;
        }
        GradeModel model = (GradeModel) attribute;
        if (model == null)
        {
            model = new GradeModel();
            context.setAttribute("gradeModel", model);
        }
        return model;
    }

    /**
     * Helper method to easily get subject by id in parameter of request
     *
     * @param request           current request which are used to get subject id parameter
     * @param subjectRepository repository to search subject by id
     * @return subject
     */
    public static Subject getSubjectByParameter(HttpServletRequest request, SubjectRepository subjectRepository)
    {
        String subjectIdText = request.getParameter("subject-id");
        try
        {
            int subjectId = Integer.parseInt(subjectIdText);
            return subjectRepository.findById(subjectId);
        }
        catch (NumberFormatException e)
        {
            // return null and react in servlet to show error
            return null;
        }
    }

    /**
     * Helper method to easily get cookie by name from request
     *
     * @param request current request which are used to get cookie
     * @param name name of cookie (case sensivity)
     * @return cookie value
     */
    public static String getCookieValue(HttpServletRequest request, String name)
    {
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies)
        {
            if (cookie.getName().equals(name))
            {
                return cookie.getValue();
            }
        }
        return null;
    }
}
