package pl.kliniewski.schoolregister.grade;

import pl.kliniewski.schoolregister.subject.Subject;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * This class is container of grade
 */
@Entity
public class Grade
{
    /**
     * Grade id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * Grade value
     */
    private float value;

    /**
     * Grade date of issued
     */
    private LocalDateTime dateOfIssued;

    @ManyToOne
    private Subject subject;

    public Grade()
    {
        this(0, 0, LocalDateTime.now());
    }

    public Grade(int id, float value, LocalDateTime dateOfIssued)
    {
        this.id = id;
        this.value = value;
        this.dateOfIssued = Objects.requireNonNull(dateOfIssued, "dateOfIssued");
    }

    /**
     * Grade id getter
     *
     * @return grade id
     */
    public int getId()
    {
        return id;
    }

    /**
     * Grade id setter
     *
     * @param id grade id
     */
    public void setId(int id)
    {
        this.id = id;
    }

    /**
     * Grade id getter
     *
     * @return grade value
     */
    public float getValue()
    {
        return value;
    }

    /**
     * Grade value setter
     *
     * @param value grade value
     */
    public void setValue(float value)
    {
        this.value = value;
    }

    /**
     * Grade date of issued getter
     *
     * @return grade date of issued
     */
    public LocalDateTime getDateOfIssued()
    {
        return dateOfIssued;
    }

    /**
     * Grade date of issued setter
     *
     * @param dateOfIssued grade date of issued
     */
    public void setDateOfIssued(LocalDateTime dateOfIssued)
    {
        this.dateOfIssued = dateOfIssued;
    }

    public Subject getSubject()
    {
        return subject;
    }

    public void setSubject(Subject subject)
    {
        this.subject = subject;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (!(o instanceof Grade))
        {
            return false;
        }
        Grade grade = (Grade) o;
        return id == grade.id;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id);
    }
}
