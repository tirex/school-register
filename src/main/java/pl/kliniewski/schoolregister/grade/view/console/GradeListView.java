package pl.kliniewski.schoolregister.grade.view.console;

import pl.kliniewski.schoolregister.grade.Grade;
import pl.kliniewski.schoolregister.view.console.ConsoleTableView;

import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Objects;

/**
 * Grade list view
 */
public class GradeListView
        extends ConsoleTableView<Grade>
{
    /**
     * Format of date which is displayed in view
     */
    private final DateTimeFormatter dateTimeFormatter;

    /**
     * Collection of grades to display
     */
    private Collection<Grade> grades;

    public GradeListView(DateTimeFormatter dateTimeFormatter)
    {
        this.dateTimeFormatter = Objects.requireNonNull(dateTimeFormatter, "dateTimeFormatter");

        this.addColumn(3, "Id", grade -> Integer.toString(grade.getId()));
        this.addColumn(5, "Ocena", grade -> Float.toString(grade.getValue()));
        this.addColumn(18, "Data nadania", grade -> this.dateTimeFormatter.format(grade.getDateOfIssued()));
    }

    @Override
    public String getTitle()
    {
        return "Lista ocen studenta";
    }

    @Override
    public Collection<Grade> getRows()
    {
        return this.grades;
    }

    /**
     * Show grades average
     *
     * @param average grades average
     */
    public void showGradesAverage(double average)
    {
        System.out.println("Średnia ocen studenta wynosi: " + String.format("%.2f", average));
    }

    /**
     * Show grade bad id error message
     */
    public void showGradeBadIdError()
    {
        System.out.println("Podano nieprawidłowe id oceny do usunięcia.");
    }

    /**
     * Show empty grade list error message
     */
    public void showEmptyGradeListError()
    {
        System.out.println("Nie można policzyć średniej dla pustej listy ocen");
    }

    /**
     * Show information about waiting and wait
     */
    public void showResult()
    {
        System.out.println("Wciśnij enter żeby kontynuować");
    }

    /**
     * Show message that asks user for grade
     */
    public void askForGrade()
    {
        System.out.print("Podaj ocenę: ");
    }

    /**
     * Show message that asks user for grade id
     */
    public void askForGradeId()
    {
        System.out.print("Podaj ocenę: ");
    }

    /**
     * Set grades to be displayed
     *
     * @param grades collection of grades
     */
    public void setGrades(Collection<Grade> grades)
    {
        this.grades = grades;
    }
}
