package pl.kliniewski.schoolregister.grade.exception;

/**
 * Exception which is throwing by grades logic
 */
public class GradeListEmptyException
        extends Exception
{
    public GradeListEmptyException()
    {
    }
}
