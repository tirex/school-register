package pl.kliniewski.schoolregister.grade.repository;

import pl.kliniewski.schoolregister.grade.Grade;
import pl.kliniewski.schoolregister.subject.Subject;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Implementation of grade in memory repository
 */
public class GradeInMemoryRepository
        implements GradeRepository
{
    /**
     * Map which stores grade map by id
     */
    private final Map<Integer, Grade> gradeMap = new HashMap<>();

    /**
     * Next grade id to use when saving
     */
    private int nextGradeId = 1;

    @Override
    public Collection<Grade> findAll()
    {
        return this.gradeMap.values();
    }

    @Override
    public Grade findById(Integer id)
    {
        return this.gradeMap.get(id);
    }

    @Override
    public void save(Grade value)
    {
        if (!this.gradeMap.containsValue(value))
        {
            value.setId(this.nextGradeId++);
            this.gradeMap.put(value.getId(), value);
        }
    }

    @Override
    public void deleteById(Integer id)
    {
        Grade grade = this.findById(id);
        if (grade != null)
        {
            this.delete(grade);
        }
    }

    @Override
    public void delete(Grade value)
    {
        Subject subject = value.getSubject();
        if (subject != null)
        {
            subject.getGrades().remove(value);
        }
        this.gradeMap.remove(value.getId());
    }
}
