package pl.kliniewski.schoolregister.grade.repository;

import pl.kliniewski.schoolregister.grade.Grade;
import pl.kliniewski.schoolregister.Repository;

/**
 * Grades repository
 */
public interface GradeRepository
        extends Repository<Integer, Grade>
{
}
