package pl.kliniewski.schoolregister.grade.repository;

import pl.kliniewski.schoolregister.grade.Grade;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collection;
import java.util.Objects;

public class GradeJpaRepository
        implements GradeRepository
{
    private final EntityManager entityManager;

    public GradeJpaRepository(EntityManager entityManager)
    {
        this.entityManager = Objects.requireNonNull(entityManager, "entityManager");
    }

    @Override
    public Collection<Grade> findAll()
    {
        CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();
        CriteriaQuery<Grade> query = cb.createQuery(Grade.class);
        Root<Grade> root = query.from(Grade.class);
        CriteriaQuery<Grade> all = query.select(root);
        TypedQuery<Grade> typedQuery = this.entityManager.createQuery(all);
        return typedQuery.getResultList();
    }

    @Override
    public Grade findById(Integer id)
    {
        return this.entityManager.find(Grade.class, id);
    }

    @Override
    public void save(Grade value)
    {
        this.entityManager.getTransaction().begin();
        try
        {
            this.entityManager.persist(value);
            this.entityManager.getTransaction().commit();
        }
        catch (PersistenceException e)
        {
            e.printStackTrace();
            this.entityManager.getTransaction().rollback();
        }
    }

    @Override
    public void deleteById(Integer id)
    {
        this.entityManager.getTransaction().begin();
        try
        {
            this.entityManager.remove(this.entityManager.find(Grade.class, id));
            this.entityManager.getTransaction().commit();
        }
        catch (PersistenceException e)
        {
            e.printStackTrace();
            this.entityManager.getTransaction().rollback();
        }
    }

    @Override
    public void delete(Grade value)
    {
        this.entityManager.getTransaction().begin();
        try
        {
            this.entityManager.remove(value);
            this.entityManager.getTransaction().commit();
        }
        catch (PersistenceException e)
        {
            e.printStackTrace();
            this.entityManager.getTransaction().rollback();
        }
    }
}
