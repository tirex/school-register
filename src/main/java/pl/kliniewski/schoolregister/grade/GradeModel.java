package pl.kliniewski.schoolregister.grade;

import pl.kliniewski.schoolregister.grade.exception.GradeListEmptyException;

import java.util.Collection;

/**
 * Model which contains methods of grades logic
 */
public class GradeModel
{
    /**
     * Method return average of grades
     *
     * @param grades collection of grades
     * @return average
     * @throws GradeListEmptyException throw when collection of grades is empty
     */
    public double computeGradesAverage(Collection<Grade> grades) throws GradeListEmptyException
    {
        return grades.stream().mapToDouble(Grade::getValue).average().orElseThrow(GradeListEmptyException::new);
    }
}
