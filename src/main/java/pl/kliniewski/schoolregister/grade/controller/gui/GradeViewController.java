package pl.kliniewski.schoolregister.grade.controller.gui;

import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import pl.kliniewski.schoolregister.grade.Grade;
import pl.kliniewski.schoolregister.grade.GradeModel;
import pl.kliniewski.schoolregister.grade.exception.GradeListEmptyException;
import pl.kliniewski.schoolregister.grade.repository.GradeRepository;

import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.ResourceBundle;

/**
 * Grade View controller of gui
 */
public class GradeViewController
        implements Initializable
{
    /**
     * Grade model
     */
    private final GradeModel gradeModel;

    /**
     * Grade repository
     */
    private final GradeRepository gradeRepository;

    /**
     * Date formatter
     */
    private final DateTimeFormatter dateTimeFormatter;

    /**
     * TableView id column
     */
    @FXML
    private TableColumn<Grade, String> tableIdColumn;

    /**
     * TableView value column
     */
    @FXML
    private TableColumn<Grade, String> tableValueColumn;

    /**
     * TableView date column
     */
    @FXML
    private TableColumn<Grade, String> tableDateColumn;

    /**
     * TableView of grades
     */
    @FXML
    private TableView<Grade> gradeTableView;

    /**
     * New grade value text field
     */
    @FXML
    private TextField gradeTextField;

    /**
     * Grades average label
     */
    @FXML
    private Label averageLabel;

    public GradeViewController(GradeModel gradeModel, GradeRepository gradeRepository,
            DateTimeFormatter dateTimeFormatter)
    {
        this.gradeModel = Objects.requireNonNull(gradeModel, "gradeModel");
        this.gradeRepository = Objects.requireNonNull(gradeRepository, "gradeRepository");
        this.dateTimeFormatter = Objects.requireNonNull(dateTimeFormatter, "dateTimeFormatter");
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle)
    {
        this.tableIdColumn.setCellValueFactory(
                data -> new SimpleStringProperty(Integer.toString(data.getValue().getId())));
        this.tableValueColumn.setCellValueFactory(
                data -> new SimpleStringProperty(Float.toString(data.getValue().getValue())));
        this.tableDateColumn.setCellValueFactory(
                data -> new SimpleStringProperty(this.dateTimeFormatter.format(data.getValue().getDateOfIssued())));

        this.updateGradesView();
    }

    /**
     * Button action that add new grade
     */
    @FXML
    public void addGrade()
    {
        try
        {
            float value = Float.parseFloat(this.gradeTextField.getText());
            Grade grade = new Grade(0, value, LocalDateTime.now());
            this.gradeRepository.save(grade);

            this.updateGradesView();
        }
        catch (NumberFormatException ignored)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Błąd");
            alert.setContentText("Podano nieprawidłową ocenę");
            alert.showAndWait();
        }
    }

    /**
     * Button action that delete selected grade
     */
    @FXML
    public void deleteGrade()
    {
        Grade grade = this.gradeTableView.getSelectionModel().getSelectedItem();
        if (grade != null)
        {
            this.gradeRepository.delete(grade);

            this.updateGradesView();
        }
    }

    /**
     * Update grades view using model
     */
    public void updateGradesView()
    {
        this.updateTableView();
        this.updateAverageView();
    }

    /**
     * Update grades in table view
     */
    public void updateTableView()
    {
        this.gradeTableView.getItems().setAll(this.gradeRepository.findAll());
    }

    /**
     * Update grades average in label
     */
    public void updateAverageView()
    {
        try
        {
            this.averageLabel.setText(
                    String.format("%.2f", this.gradeModel.computeGradesAverage(this.gradeRepository.findAll())));
        }
        catch (GradeListEmptyException e)
        {
            this.averageLabel.setText("Nie można wyliczyć średniej");
        }
    }
}
