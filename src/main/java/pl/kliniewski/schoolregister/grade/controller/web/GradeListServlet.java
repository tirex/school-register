package pl.kliniewski.schoolregister.grade.controller.web;

import pl.kliniewski.schoolregister.grade.Grade;
import pl.kliniewski.schoolregister.grade.repository.GradeRepository;
import pl.kliniewski.schoolregister.subject.Subject;
import pl.kliniewski.schoolregister.subject.repository.SubjectRepository;
import pl.kliniewski.schoolregister.util.ServletUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;

@WebServlet(name = "gradeListServlet", urlPatterns = "/grade-list")
public class GradeListServlet
        extends HttpServlet
{
    private GradeRepository gradeRepository;
    private SubjectRepository subjectRepository;

    @Override
    public void init(ServletConfig config)
    {
        ServletContext servletContext = config.getServletContext();
        this.gradeRepository = ServletUtils.getGradeRepository(servletContext);
        this.subjectRepository = ServletUtils.getSubjectRepository(servletContext, this.gradeRepository);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Subject subject = ServletUtils.getSubjectByParameter(request, this.subjectRepository);
        if (subject == null)
        {
            RequestDispatcher dispatcher = request.getRequestDispatcher("no-subject-error.jsp");
            dispatcher.forward(request, response);
            return;
        }
        request.setAttribute("subject", subject);
        request.setAttribute("grades", subject.getGrades());

        RequestDispatcher dispatcher = request.getRequestDispatcher("grade-list.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if (request.getParameter("delete") != null)
        {
            String idText = request.getParameter("id");
            int id = Integer.parseInt(idText);
            this.gradeRepository.deleteById(id);

            request.setAttribute("deleteSuccess", true);
            this.doGet(request, response);
            return;
        }
        Subject subject = ServletUtils.getSubjectByParameter(request, this.subjectRepository);
        if (subject == null)
        {
            RequestDispatcher dispatcher = request.getRequestDispatcher("no-subject-error.jsp");
            dispatcher.forward(request, response);
            return;
        }

        String valueText = request.getParameter("value");
        try
        {
            float value = Float.parseFloat(valueText);

            Grade grade = new Grade(0, value, LocalDateTime.now());
            grade.setSubject(subject);
            this.gradeRepository.save(grade);

            subject.getGrades().add(grade);
        }
        catch (NumberFormatException e)
        {
            request.setAttribute("badNumberFormat", true);
        }
        this.doGet(request, response);
    }
}
