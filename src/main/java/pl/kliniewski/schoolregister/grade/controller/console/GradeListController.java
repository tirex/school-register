package pl.kliniewski.schoolregister.grade.controller.console;

import pl.kliniewski.schoolregister.Controller;
import pl.kliniewski.schoolregister.grade.Grade;
import pl.kliniewski.schoolregister.grade.GradeModel;
import pl.kliniewski.schoolregister.grade.exception.GradeListEmptyException;
import pl.kliniewski.schoolregister.grade.repository.GradeRepository;
import pl.kliniewski.schoolregister.grade.view.console.GradeListView;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Scanner;

/**
 * Grade list controller
 */
public class GradeListController
        implements Controller
{
    /**
     * Grade list view
     */
    private final GradeListView view;

    /**
     * Grade model
     */
    private final GradeModel gradeModel;

    /**
     * Grade repository
     */
    private final GradeRepository gradeRepository;

    public GradeListController(GradeListView view, GradeModel gradeModel, GradeRepository gradeRepository)
    {
        this.view = Objects.requireNonNull(view, "view");
        this.gradeModel = Objects.requireNonNull(gradeModel, "gradeModel");
        this.gradeRepository = Objects.requireNonNull(gradeRepository, "gradeRepository");

        this.view.addAction("Wprowadź ocenę", this::addGrade);
        this.view.addAction("Usuń ocenę", this::deleteGrade);
        this.view.addAction("Pokaż średnią", this::showGradesAverage);
        this.view.addAction("Zakończ aplikację", () ->
        {
        });
    }

    /**
     * Add new grade
     */
    public void addGrade()
    {
        Grade grade = this.nextGrade();
        this.gradeRepository.save(grade);
        this.updateView();
    }

    /**
     * Delete one grade by id
     */
    public void deleteGrade()
    {
        int gradeId = this.nextGradeId();
        Grade grade = this.gradeRepository.findById(gradeId);
        if (grade != null)
        {
            this.gradeRepository.delete(grade);
        }
        else
        {
            this.view.showGradeBadIdError();
        }
        this.showResultAndWait();
        this.updateView();
    }

    /**
     * Show grade average
     */
    public void showGradesAverage()
    {
        try
        {
            double average = this.gradeModel.computeGradesAverage(this.gradeRepository.findAll());
            this.view.showGradesAverage(average);
        }
        catch (GradeListEmptyException e)
        {
            this.view.showEmptyGradeListError();
            this.showResultAndWait();
        }
        this.updateView();
    }

    @Override
    public GradeListView getView()
    {
        return view;
    }

    @Override
    public void updateView()
    {
        this.view.setGrades(this.gradeRepository.findAll());
        this.view.print();
        this.view.waitForAction();
    }

    /**
     * Show result and wait for user action
     */
    public void showResultAndWait()
    {
        this.view.showResult();

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
    }

    /**
     * Get a grade from a user
     *
     * @return instance of new grade
     */
    public Grade nextGrade()
    {
        Scanner scanner = new Scanner(System.in);
        this.view.askForGrade();

        float value = scanner.nextFloat();
        return new Grade(0, value, LocalDateTime.now());
    }

    /**
     * Ask for grade id and get
     *
     * @return grade id
     */
    public int nextGradeId()
    {
        Scanner scanner = new Scanner(System.in);

        this.view.askForGradeId();
        return scanner.nextInt();
    }
}
