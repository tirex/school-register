package pl.kliniewski.schoolregister.grade.controller.web;

import pl.kliniewski.schoolregister.grade.GradeModel;
import pl.kliniewski.schoolregister.grade.exception.GradeListEmptyException;
import pl.kliniewski.schoolregister.grade.repository.GradeRepository;
import pl.kliniewski.schoolregister.subject.Subject;
import pl.kliniewski.schoolregister.subject.repository.SubjectRepository;
import pl.kliniewski.schoolregister.util.ServletUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "gradesAverageServlet", urlPatterns = "/grades-average")
public class GradesAverageServlet
        extends HttpServlet
{
    private SubjectRepository subjectRepository;
    private GradeModel gradeModel;

    @Override
    public void init(ServletConfig config)
    {
        ServletContext servletContext = config.getServletContext();
        GradeRepository gradeRepository = ServletUtils.getGradeRepository(servletContext);
        this.subjectRepository = ServletUtils.getSubjectRepository(servletContext, gradeRepository);
        this.gradeModel = ServletUtils.getGradeModel(servletContext);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Subject subject = ServletUtils.getSubjectByParameter(request, this.subjectRepository);
        if (subject == null)
        {
            RequestDispatcher dispatcher = request.getRequestDispatcher("no-subject-error.jsp");
            dispatcher.forward(request, response);
            return;
        }
        request.setAttribute("lastGradesAverage", ServletUtils.getCookieValue(request, "lastGradesAverage"));

        request.setAttribute("subject", subject);
        try
        {
            double average = this.gradeModel.computeGradesAverage(subject.getGrades());
            request.setAttribute("gradesAverage", average);

            Cookie cookie = new Cookie("lastGradesAverage", Double.toString(average));
            response.addCookie(cookie);
        }
        catch (GradeListEmptyException e)
        {
            request.setAttribute("gradesAverageError", true);
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("grades-average.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        this.doGet(request, response);
    }
}
