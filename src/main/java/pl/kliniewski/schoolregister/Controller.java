package pl.kliniewski.schoolregister;

import pl.kliniewski.schoolregister.view.View;

/**
 * Application controller
 */
public interface Controller
{
    /**
     * Update view which is controlled by controller
     */
    void updateView();

    /**
     * Get view which is controlled by controller
     *
     * @return view controlled by controller
     */
    View getView();

    /**
     * Controller start point
     */
    default void start()
    {
        this.updateView();
    }
}
