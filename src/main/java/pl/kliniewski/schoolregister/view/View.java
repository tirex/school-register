package pl.kliniewski.schoolregister.view;

/**
 * Application view
 */
public interface View
{
    /**
     * Show view
     */
    void show();
}
