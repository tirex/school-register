package pl.kliniewski.schoolregister.view.console;

/**
 * Represents a callback that can be performed
 */
@FunctionalInterface
public interface ActionCallback
{
    /**
     * Method which will be invoked after action was performed
     */
    void run();
}
