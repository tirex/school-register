package pl.kliniewski.schoolregister.view.console;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.StringJoiner;
import java.util.function.Function;

/**
 * View which print table in console
 *
 * @param <T> type of console table value
 */
public abstract class ConsoleTableView<T>
        implements ConsoleView, Actionable
{
    /**
     * Columns in table
     */
    private final List<TableColumn> columns = new ArrayList<>();

    /**
     * Action which can be called by action id
     */
    private final Map<Integer, Action> actionMap = new HashMap<>();

    /**
     * Next action id to fill index of action
     */
    private int nextActionId;

    /**
     * Method which return a table title to be displayed in table header
     *
     * @return table title
     */
    public abstract String getTitle();

    /**
     * Method which return a collection of rows to be displayed in table
     *
     * @return collection of rows
     */
    public abstract Collection<T> getRows();

    /**
     * Add new column to table view
     *
     * @param width                width of column in whitespace characters
     * @param headerName           name of column which will be displayed in table
     * @param extractValueFunction extractor of column value
     */
    public void addColumn(int width, String headerName, Function<T, String> extractValueFunction)
    {
        this.columns.add(new TableColumn(width, headerName, extractValueFunction));
    }

    @Override
    public void print()
    {
        String title = this.getTitle();
        if (title != null)
        {
            System.out.print("| ");
            System.out.print(this.centeredString(title, this.getTableWidth()));
            System.out.println(" |");
        }
        this.printHeader();

        Collection<T> rows = this.getRows();
        if (rows == null || rows.isEmpty())
        {
            System.out.print("| ");
            System.out.print(this.centeredString("Brak danych", this.getTableWidth()));
            System.out.println(" |");
            System.out.println();
            return;
        }
        for (T row : rows)
        {
            StringJoiner joiner = new StringJoiner(" | ");
            for (TableColumn column : this.columns)
            {
                String text = column.getExtractValueFunction().apply(row);
                joiner.add(String.format("%-" + column.getWidth() + "s", text));
            }
            System.out.println("| " + joiner + " |");
        }
        System.out.println();
    }

    @Override
    public void addAction(String name, ActionCallback callback)
    {
        Action action = new Action(this.nextActionId++, name, callback);
        this.actionMap.put(action.getId(), action);
    }

    @Override
    public void waitForAction()
    {
        if (this.actionMap.isEmpty())
        {
            return;
        }
        System.out.println("Co chcesz zrobić?");
        this.actionMap.forEach((id, action) -> System.out.println(id + " - " + action.getName()));
        System.out.println();

        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj id akcji: ");
        int actionId = scanner.nextInt();

        Action action = this.actionMap.get(actionId);
        if (action == null)
        {
            System.out.println("Nie znaleziono akcji z id " + actionId);
            System.out.println();
            this.waitForAction();
            return;
        }
        action.getCallback().run();
    }

    /**
     * Compute full table width with columns width
     *
     * @return table width
     */
    public int getTableWidth()
    {
        int width = !this.columns.isEmpty() ? -3 : 0;
        for (TableColumn column : this.columns)
        {
            width += column.width;
            width += 3; // separator length
        }
        return width;
    }

    /**
     * Print header of table
     */
    private void printHeader()
    {
        StringJoiner joiner = new StringJoiner(" | ");
        for (TableColumn column : this.columns)
        {
            joiner.add(String.format("%-" + column.getWidth() + "s", column.getHeaderName()));
        }
        System.out.println("| " + joiner + " |");
    }

    /**
     * Create string which is centered to be aligned to specific width
     *
     * @param text text to be centered
     * @param width generated text width
     * @return generated text
     */
    private String centeredString(String text, int width) throws ConsoleTableException
    {
        if (text.length() > width)
        {
            throw new ConsoleTableException("Centered text.length() should be a shorter than width");
        }
        int fillLength = width - text.length();
        int firstPartLength = fillLength / 2;
        int secondPartLength = width - text.length() - firstPartLength;
        return " ".repeat(firstPartLength) + text + " ".repeat(secondPartLength);
    }

    /**
     * Table column container
     */
    class TableColumn
    {
        /**
         * Column width
         */
        private final int width;

        /**
         * Column header name
         */
        private final String headerName;

        /**
         * Column value extractor
         */
        private final Function<T, String> extractValueFunction;

        public TableColumn(int width, String headerName, Function<T, String> extractValueFunction)
        {
            this.width = width;
            this.headerName = headerName;
            this.extractValueFunction = extractValueFunction;
        }

        /**
         * Column width getter
         *
         * @return column width
         */
        public int getWidth()
        {
            return width;
        }

        /**
         * Column header name getter
         *
         * @return column header name
         */
        public String getHeaderName()
        {
            return headerName;
        }

        /**
         * Column header value extractor
         *
         * @return value extractor
         */
        public Function<T, String> getExtractValueFunction()
        {
            return extractValueFunction;
        }
    }

    /**
     * Table action container
     */
    static class Action
    {
        /**
         * Action id
         */
        private final int id;

        /**
         * Action name
         */
        private final String name;

        /**
         * Action callback
         */
        private final ActionCallback callback;

        public Action(int id, String name, ActionCallback callback)
        {
            this.id = id;
            this.name = name;
            this.callback = callback;
        }

        /**
         * Action id getter
         *
         * @return action id
         */
        public int getId()
        {
            return id;
        }

        /**
         * Action name getter
         *
         * @return action name
         */
        public String getName()
        {
            return name;
        }

        /**
         * Action callback getter
         *
         * @return action callback
         */
        public ActionCallback getCallback()
        {
            return callback;
        }
    }
}
