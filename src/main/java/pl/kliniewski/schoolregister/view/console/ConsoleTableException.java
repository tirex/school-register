package pl.kliniewski.schoolregister.view.console;

/**
 * Exception in console table implementation
 */
public class ConsoleTableException
        extends RuntimeException
{
    public ConsoleTableException(String message)
    {
        super(message);
    }
}
