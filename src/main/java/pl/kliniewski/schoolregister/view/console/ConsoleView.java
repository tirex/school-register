package pl.kliniewski.schoolregister.view.console;

import pl.kliniewski.schoolregister.view.View;

/**
 * Interface which allow to show console ui
 */
public interface ConsoleView
        extends View
{
    /**
     * Print content of ConsoleView
     */
    void print();

    @Override
    default void show()
    {
        this.print();
    }
}
