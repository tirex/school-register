package pl.kliniewski.schoolregister.view.console;

/**
 * Interface which allow view to be actionable
 */
public interface Actionable
{
    /**
     * Add new action with specified name and callback
     *
     * @param name action name
     * @param callback action performed when action is invoked
     */
    void addAction(String name, ActionCallback callback);

    /**
     * Wait for action from the user
     */
    void waitForAction();
}
