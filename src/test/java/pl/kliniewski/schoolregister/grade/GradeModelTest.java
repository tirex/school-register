package pl.kliniewski.schoolregister.grade;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import pl.kliniewski.schoolregister.grade.exception.GradeListEmptyException;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class GradeModelTest
{
    private GradeModel gradeModel;

    @BeforeEach
    void setUp()
    {
        this.gradeModel = new GradeModel();
    }

    /**
     * Test will check the correct value of computed average
     *
     * @param gradeList grade list which will be used to compute average
     * @param expectedAverage expected average of grades
     */
    @ParameterizedTest
    @MethodSource
    public void testComputeGradesAverage(List<Number> gradeList, double expectedAverage)
    {
        Collection<Grade> grades = gradeList.stream()
                .map(grade -> new Grade(1, grade.floatValue(), LocalDateTime.now()))
                .collect(Collectors.toList());

        assertDoesNotThrow(() ->
        {
            assertEquals(expectedAverage, this.gradeModel.computeGradesAverage(grades), 0.01,
                    "Incorrectly calculated average");
        }, "Unexpected exception in usual counting of numbers");
    }

    /**
     * Test will check if the method throw exception for an empty list of grades
     */
    @Test
    public void testComputeGradesAverageEmptyGradeList()
    {
        assertThrows(GradeListEmptyException.class, () -> this.gradeModel.computeGradesAverage(Collections.emptyList()),
                "Expected exception when trying to compute an empty grade list");
    }

    /**
     * Method source for testComputeGradesAverage parameterized test
     *
     * @return arguments for parameterized test
     */
    public static Stream<Arguments> testComputeGradesAverage()
    {
        return Stream.of(Arguments.of(Arrays.asList(5, 4), 4.5), Arguments.of(Arrays.asList(5, 4, 4), 4.33),
                Arguments.of(Arrays.asList(0, 0), 0), Arguments.of(Arrays.asList(5, 5), 5),
                Arguments.of(Arrays.asList(5, 0), 2.5), Arguments.of(Arrays.asList(5, 1), 3));
    }
}
